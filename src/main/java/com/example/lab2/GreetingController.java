package com.example.lab2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;

@RestController
public class GreetingController {

    @GetMapping("/greetings")
    public String getGreeting() {
        int time = Integer.parseInt(LocalTime.now().toString().substring(0,2));
        if (time < 12) {
            return "Good Morning! It is now: " + LocalDate.now().toString() + ", " + LocalTime.now().toString();
        } else if (time < 17) {
            return "Good Afternoon! It is now: " + LocalDate.now().toString() + ", " + LocalTime.now().toString();
        } else {
            return "Good Evening! It is now: " + LocalDate.now().toString() + ", " + LocalTime.now().toString();
        }
    }

}
